---
title: "R Notebook"
output: html_notebook
---

```{r}
library("haven")
Baza <- read_sav("../data/input/wyniki-surowe-afiliacja.sav")
```

```{r}
library("psych")
```

```{r}
              psych::describe(Baza$total.AN)
psych::describe(Baza$total.ES)
psych::describe(Baza$total.SC)
psych::describe(Baza$total.PAS)
psych::describe(Baza$total.PS)
psych::describe(Baza) -> zmienne_opisowe
```



```{r}
ggplot(data = zmienne_opisowe) + geom_boxplot(aes(x = zmienne_opisowe$range))
  
```

```{r}
ggplot(data = zmienne_opisowe) +
  geom_histogram(
    aes(
      x = zmienne_opisowe$range,
      fill = "magenta"
    )
  )
```
```{r}
an <- stats::aov(Wiek ~ Wykształcenie, data = Baza)
summary(an)
```

