---
title: "zabawa-z-r-jarek"
output: html_document
date: "2023-06-26"
---
3.
```{r}
library("tidyverse")
ggplot(mpg) +
  geom_point(mapping = 
               aes(x = displ,
y = hwy))
```
```{r}
ggplot(mpg) +
  geom_point(mapping = 
               aes(x = hwy,
y = cyl))
```
```{r}
ggplot(mpg) +
  geom_point(mapping = 
               aes(x = class,
y = drv))
```
```{r}
ggplot(mpg) +
  geom_point(
    mapping = aes(
      x = displ,
      y = hwy,
      color = class
    )
  )
```
```{r}
ggplot(mpg)+
  geom_point(
    mapping = aes(
      x = displ,
      y = hwy,
      alpha = class
    )
  )
```
```{r}
ggplot(mpg)+
  geom_point(
    mapping = aes(
      x = displ,
      y = hwy,
      shape = class
    )
  )
```
```{r}
ggplot(mpg)+
  geom_point(
    mapping = aes(
      x = displ,
      y = hwy),
      color = "blue"
    )
  
```

```{r}
ggplot(data = mpg) + 
  geom_point(mapping = aes(x = displ, y = hwy), color = "blue")
```
```{r}
ggplot(mpg)+
  geom_point(
    mapping = aes(
      x = displ,
      y = hwy),
      color = "blue",
    size = 3,
    shape = 10
    )
```
```{r}
ggplot(mpg) +
  geom_point(mapping =
               aes(
                 x = displ,
                 y = hwy)) +
  facet_grid(drv ~cyl)
```
```{r}
ggplot(mpg) +
  geom_point(mapping =
               aes(
                 x = displ,
                 y = hwy)) +
  facet_grid(. ~cyl)
```
```{r}
ggplot(mpg) +
  geom_point(
    mapping =
      aes(
        x = displ,
        y = hwy
      ) 
  ) +
  facet_wrap(~ class,
             nrow = 2)
```
```{r}
ggplot(mpg) +
  geom_point(mapping =
               aes
             (x = drv,
               y = cyl)) +
  facet_grid(drv ~ cyl)
```
```{r}
ggplot(mpg) +
  geom_point(mapping
             = aes
             (x = displ,
               y = hwy)) + facet_grid(drv ~ .)
```

```{r}
ggplot(mpg) +
  geom_point(
    mapping
    = aes(
      x = displ,
      y = hwy)) + facet_grid(. ~ cyl)

```
```{r}
ggplot(data = mpg) + 
  geom_point(mapping = aes(x = displ, y = hwy)) + 
  facet_wrap(~ class, nrow = 2)
```
```{r}
ggplot(mpg) +
  geom_point(
    mapping =
      aes(
      x = displ,
      y = hwy
    )
  )
```
```{r}
ggplot(mpg) +
  geom_smooth(
    mapping =
      aes(
      x = displ,
      y = hwy,
      linetype = drv
    )
  )
```
```{r}
ggplot(mpg) +
  geom_smooth(
    mapping = 
      aes(
        x = displ,
        y = hwy
      )
  )
  ggplot(mpg) +
  geom_smooth(
    mapping =
      aes(
        x = displ,
        y = hwy,
        group = drv
      )
  )
  ggplot(mpg) +
    geom_smooth(
      mapping =
        aes(
          x = displ,
          y = hwy,
          color = drv,
         )
    )
```
```{r}
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy)) +
  geom_smooth(mapping = aes(x = displ, y = hwy))


```
```{r}
ggplot(data = mpg, mapping = aes(x = displ, y = hwy)) +
  geom_point(mapping = aes(color = class)) +
  geom_smooth(data = filter(mpg, class == "subcompact"), se = FALSE)
```
```{r}
ggplot(data = mpg,
       mapping = aes(x = displ, y = hwy, color = drv)) +
  geom_point() +
  geom_smooth(se = FALSE)
```
```{r}
ggplot(data = mpg, mapping = aes(x = displ, y = hwy)) + 
  geom_point() + 
  geom_smooth()
```
```{r}
ggplot() + 
  geom_point(data = mpg, mapping = aes(x = displ, y = hwy)) + 
  geom_smooth(data = mpg, mapping = aes(x = displ, y = hwy))
```
```{r}
ggplot(mpg) + geom_point(aes(x = displ, y = hwy), size = 5) + geom_smooth(aes(x = displ, y = hwy))
```
```{r}
ggplot(mpg) + geom_point(aes(x = displ, y = hwy), size = 5) + geom_smooth(aes(x = cyl, y = hwy))+ geom_smooth(aes(x = displ, y = cyl))
```
```{r}
ggplot(diamonds) +geom_bar(aes(x = cut))
```
```{r}
ggplot(diamonds) + stat_count(aes(x = cut))
```

```{r}
ggplot(diamonds) +
  geom_bar(aes(x = cut, fill = clarity))
```
```{r}
ggplot(data = diamonds, mapping = aes(x = cut, fill = clarity)) + 
  geom_bar(alpha = 1/5, position = "identity")
ggplot(data = diamonds, mapping = aes(x = cut, colour = clarity)) + 
  geom_bar(fill = NA, position = "identity")
```
```{r}
ggplot(data = diamonds) + 
  geom_bar(mapping = aes(x = cut, fill = clarity), position = "dodge")
```
```{r}
ggplot(data = mpg) + 
  geom_point(mapping = aes(x = displ, y = hwy), position = "jitter")
```
```{r}
ggplot(mpg) +
  geom_jitter(aes(x = displ, y = hwy))
```
```{r}
ggplot(mpg, aes(x = cty, y = hwy)) + 
  geom_jitter()
```
```{r}
ggplot(mpg, aes(x = cty, y = hwy)) + 
  geom_count()
```

```{r}
ggplot(data = mpg, mapping = aes(x = class, y = hwy)) + 
  geom_boxplot()
ggplot(data = mpg, mapping = aes(x = class, y = hwy)) + 
  geom_boxplot() +
  coord_flip()
```
```{r}
nz <- map_data("nz")

ggplot(nz, aes(long, lat, group = group)) +
  geom_polygon(fill = "white", colour = "black")

ggplot(nz, aes(long, lat, group = group)) +
  geom_polygon(fill = "white", colour = "black") +
  coord_quickmap()
```
```{r}
bar <- ggplot(data = diamonds) + 
  geom_bar(
    mapping = aes(x = cut, fill = cut), 
    show.legend = FALSE,
    width = 1
  ) + 
  theme(aspect.ratio = 1) +
  labs(x = NULL, y = NULL)

bar + coord_flip()
bar + coord_polar()
```
```{r}
ggplot(data = mpg, mapping = aes(x = cty, y = hwy)) +
  geom_point() + 
  geom_abline() +
  coord_fixed()
```

```{r}
library(nycflights13)
library(dplyr)
view(flights)
```

```{r}
jan1 <- filter(flights, month == 1, day == 1)
(dec25 <- filter(flights, month == 12, day == 25))
```

```{r}
nov_dec <- filter(flights, month %in% c(11, 12))
view(nov_dec)
```

```{r}
 delayed<- filter(flights, flights$arr_delay >= 2)
view(delayed)
```
```{r}
destination <- subset(flights, dest %in% c("IAH", "HOU"))
view(destination)
```

```{r}
carrier <- subset(flights, carrier %in% c("UA", "AA", "DL"))
view(carrier)
```

```{r}
flights_f <- subset(flights, month %in% c(7, 8, 9))
view(flights_f)
```

```{r}
delay <- subset(flights, arr_delay > 0 & dep_delay == 0)
view(delay)
```

```{r}
arrange(flights, year, month, day)
arrange(flights, desc(dep_delay))
```

```{r}
is.na <- arrange(flights, desc(is.na(flights)))
view(is.na)
```

```{r}
arrange(flights, desc(dep_delay))
```
```{r}
arrange(flights, air_time)
```
```{r}
arrange(flights, desc(distance))
```

```{r}
select(flights, year, month, day)
```
```{r}
select(flights, year:day)
```
```{r}
rename(flights, tail_num = tailnum)
```
```{r}
vars <- c("year", "month", "day", "dep_delay", "arr_delay")
any_of(vars)
```
```{r}
select(flights, contains("TIME"))
```
```{r}
flights_sml <- select(flights, year:day, ends_with("delay"), 
  distance, 
  air_time)
view(flights_sml)

```

```{r}
mutate(flights_sml,
  gain = dep_delay - arr_delay,
  speed = distance / air_time * 60)
```
```{r}
transmute(flights,
  gain = dep_delay - arr_delay,
  hours = air_time / 60,
  gain_per_hour = gain / hours
)
```

```{r}
library(hms)
library(dplyr)
flights <- flights %>%
  mutate(dep_time = as.hms(sprintf("%04d", dep_time)) %>% as.numeric())
flights <- flights %>%
  mutate(sched_dep_time = as.hms(sprintf("%04d", sched_dep_time)) %>% as.numeric())
View(flights)
```

```{r}
t1 <- transmute(flights, arr_time - dep_time)
s1 <- select(flights, dep_time)
compare_tbls(s1, t1)
```
```{r}
summarise(flights, delay = mean(dep_delay, na.rm = TRUE))
```
```{r}
by_day <- group_by(flights, year, month, day)
summarise(by_day, delay = mean(dep_delay, na.rm = TRUE))
```
```{r}
by_dest <- group_by(flights, dest)
delay <- summarise(by_dest,
  count = n(),
  dist = mean(distance, na.rm = TRUE),
  delay = mean(arr_delay, na.rm = TRUE)
)
delay <- filter(delay, count > 20, dest != "HNL")

ggplot(data = delay, mapping = aes(x = dist, y = delay)) +
  geom_point(aes(size = count), alpha = 1/3) +
  geom_smooth(se = FALSE)
```
```{r}
delays <- flights %>% 
  group_by(dest) %>% 
  summarise(
    count = n(),
    dist = mean(distance, na.rm = TRUE),
    delay = mean(arr_delay, na.rm = TRUE)
  ) %>% 
  filter(count > 20, dest != "HNL")
```

```{r}
flights %>% 
  group_by(year, month, day) %>% 
  summarise(mean = mean(dep_delay))
```

```{r}
flights %>% 
  group_by(year, month, day) %>% 
  summarise(mean = mean(dep_delay, na.rm = TRUE))
```

```{r}
not_cancelled <- flights %>% 
  filter(!is.na(dep_delay), !is.na(arr_delay))

not_cancelled %>% 
  group_by(year, month, day) %>% 
  summarise(mean = mean(dep_delay))
```

```{r}
delays <- not_cancelled %>% 
  group_by(tailnum) %>% 
  summarise(
    delay = mean(arr_delay)
  )

ggplot(data = delays, mapping = aes(x = delay)) + 
  geom_freqpoly(binwidth = 10)
```

```{r}
delays <- not_cancelled %>% 
  group_by(tailnum) %>% 
  summarise(
    delay = mean(arr_delay, na.rm = TRUE),
    n = n()
  )

ggplot(data = delays, mapping = aes(x = n, y = delay)) + 
  geom_point(alpha = 1/10)
```

```{r}
delays %>% 
  filter(n > 25) %>% 
  ggplot(mapping = aes(x = n, y = delay)) + 
    geom_point(alpha = 1/10)

```

```{r}
batting <- as_tibble(Lahman::Batting)

batters <- batting %>% 
  group_by(playerID) %>% 
  summarise(
    ba = sum(H, na.rm = TRUE) / sum(AB, na.rm = TRUE),
    ab = sum(AB, na.rm = TRUE)
  )

batters %>% 
  filter(ab > 100) %>% 
  ggplot(mapping = aes(x = ab, y = ba)) +
    geom_point() + 
    geom_smooth(se = FALSE)
#> `geom_smooth()` using method = 'gam' and formula = 'y ~ s(x, bs = "cs")'
```

```{r}
not_cancelled %>% 
  group_by(year, month, day) %>% 
  summarise(
    avg_delay1 = mean(arr_delay),
    avg_delay2 = mean(arr_delay[arr_delay > 0]))
```

```{r}
not_cancelled %>% 
  group_by(dest) %>% 
  summarise(distance_sd = sd(distance)) %>% 
  arrange(desc(distance_sd))
```

```{r}
not_cancelled %>% 
  group_by(year, month, day) %>% 
  summarise(
    first = min(dep_time),
    last = max(dep_time)
  )
```

```{r}
not_cancelled %>% 
  group_by(year, month, day) %>% 
  mutate(r = min_rank(desc(dep_time))) %>% 
  filter(r %in% range(r))
```

```{r}
not_cancelled %>% 
  group_by(dest) %>% 
  summarise(carriers = n_distinct(carrier)) %>% 
  arrange(desc(carriers))
```

```{r}
not_cancelled %>% 
  count(dest)
```

```{r}
not_cancelled %>% 
  count(tailnum, wt = distance)
```

```{r}
not_cancelled %>% 
  group_by(year, month, day) %>% 
  summarise(n_early = sum(dep_time < 500))
```

```{r}
daily <- group_by(flights, year, month, day)
(per_day   <- summarise(daily, flights = n()))
```

```{r}
daily %>% 
  ungroup() %>%
  summarise(flights = n())
```

```{r}
count(not_cancelled, dest)
```

```{r}
flights %>%
  group_by(carrier, dest) %>% 
  summarise(n())
```

```{r}
flights_sml %>% 
  group_by(year, month, day) %>%
  filter(rank(desc(arr_delay)) < 10)
```

```{r}
popular_dests <- flights %>% 
  group_by(dest) %>% 
  filter(n() > 365)
popular_dests
```

```{r}
popular_dests %>% 
  filter(arr_delay > 0) %>% 
  mutate(prop_delay = arr_delay / sum(arr_delay)) %>% 
  select(year:day, dest, arr_delay, prop_delay)
```

```{r}
filter(flights, air_time > 0) %>%
  select(tailnum, air_time)
```

```{r}
smaller <- diamonds %>% 
  filter(carat < 3)
  
ggplot(data = smaller, mapping = aes(x = carat)) +
  geom_histogram(binwidth = 0.1)
```

```{r}
ggplot(data = smaller, mapping = aes(x = carat, colour = cut)) +
  geom_freqpoly(binwidth = 0.1)
```

```{r}
ggplot(data = smaller, mapping = aes(x = carat)) +
  geom_histogram(binwidth = 0.01)
```

```{r}
ggplot(data = faithful, mapping = aes(x = eruptions)) + 
  geom_histogram(binwidth = 0.25)
```

```{r}
ggplot(diamonds) + 
  geom_histogram(mapping = aes(x = y), binwidth = 0.5)
```

```{r}
ggplot(diamonds) + 
  geom_histogram(mapping = aes(x = y), binwidth = 0.5) +
  coord_cartesian(ylim = c(0, 50))
```

```{r}
unusual <- diamonds %>% 
  filter(y < 3 | y > 20) %>% 
  select(price, x, y, z) %>%
  arrange(y)
unusual
```

```{r}
summary(diamonds$x)
summary(diamonds$y)
summary(diamonds$z)
```
```{r}
ggplot(data = diamonds, aes(x = x)) +
  geom_histogram(fill = "lightblue", color = "darkblue", bins = 30) +
  labs(x = "x", y = "Frequency", title = "Distribution of x")
```

```{r}
ggplot(data = diamonds, aes(x = y)) +
  geom_histogram(fill = "lightgreen", color = "darkgreen", bins = 30) +
  labs(x = "y", y = "Frequency", title = "Distribution of y")
```

```{r}
ggplot(data = diamonds, aes(x = z)) +
  geom_histogram(fill = "lightyellow", color = "darkgoldenrod", bins = 30) +
  labs(x = "z", y = "Frequency", title = "Distribution of z")

```


```{r}
ggplot(data = diamonds, aes(x = "", y = x)) +
  geom_boxplot(fill = "lightblue", color = "darkblue") +
  labs(x = "", y = "x", title = "Boxplot of x") +
  theme_minimal()
```

```{r}
ggplot(diamonds) +
  geom_histogram(aes(x = price))
```

```{r}
ggplot(diamonds) +
  geom_histogram(aes(x = carat))
```
```{r}
select(diamonds, carat)
```
```{r}
diamonds2 <- diamonds %>% 
  filter(between(y, 3, 20))
```

```{r}
diamonds2 <- diamonds %>% 
  mutate(y = ifelse(y < 3 | y > 20, NA, y))

ggplot(data = diamonds2, mapping = aes(x = x, y = y)) + 
  geom_point()
```

```{r}
ggplot(data = diamonds2, mapping = aes(x = x, y = y)) + 
  geom_point(na.rm = TRUE)
```

```{r}
nycflights13::flights %>% 
  mutate(
    cancelled = is.na(dep_time),
    sched_hour = sched_dep_time %/% 100,
    sched_min = sched_dep_time %% 100,
    sched_dep_time = sched_hour + sched_min / 60
  ) %>% 
  ggplot(mapping = aes(sched_dep_time)) + 
    geom_freqpoly(mapping = aes(colour = cancelled), binwidth = 1/4)
```

```{r}
sum(flights, count, na.rm = TRUE)
mean(flights, count, na.rm = TRUE)
```

```{r}
ggplot(data = diamonds, mapping = aes(x = price)) + 
  geom_freqpoly(mapping = aes(colour = cut), binwidth = 500)
```

```{r}
ggplot(diamonds) + 
  geom_bar(mapping = aes(x = cut))
```

```{r}
ggplot(data = diamonds, mapping = aes(x = price, y = ..density..)) + 
  geom_freqpoly(mapping = aes(colour = cut), binwidth = 500)
```

```{r}
ggplot(data = diamonds, mapping = aes(x = cut, y = price)) +
  geom_boxplot()
```

```{r}
ggplot(data = mpg, mapping = aes(x = class, y = hwy)) +
  geom_boxplot()
```

```{r}
ggplot(data = mpg) +
  geom_boxplot(mapping = aes(x = reorder(class, hwy, FUN = median), y = hwy)) +
  coord_flip()
```

```{r}
nycflights13::flights %>% 
  mutate(
    cancelled = is.na(dep_time),
    sched_hour = sched_dep_time %/% 100,
    sched_min = sched_dep_time %% 100,
    sched_dep_time = sched_hour + sched_min / 60
  ) %>% 
  ggplot(mapping = aes(sched_dep_time)) + 
    geom_freqpoly(mapping = aes(colour = cancelled), binwidth = 1/4)
```

```{r}
ggplot(data = diamonds) +
  geom_count(mapping = aes(x = cut, y = color))
```

```{r}
diamonds %>% 
  count(color, cut)
```

```{r}
diamonds %>% 
  count(color, cut) %>%  
  ggplot(mapping = aes(x = color, y = cut)) +
    geom_tile(mapping = aes(fill = n))
```

```{r}
  ggplot(flights) +
  geom_tile(aes(x = arr_delay + dep_delay/2, y = time_hour))
```

```{r}
ggplot(data = diamonds) +
  geom_point(mapping = aes(x = carat, y = price))
```
```{r}
ggplot(data = diamonds) + 
  geom_point(mapping = aes(x = carat, y = price), alpha = 1 / 100)
```

```{r}
library(hexbin)
ggplot(data = diamonds) +
  geom_bin2d(mapping = aes(x = carat, y = price))

ggplot(data = diamonds) +
  geom_hex(mapping = aes(x = carat, y = price))
```

```{r}
ggplot(data = diamonds, mapping = aes(x = carat, y = price)) + 
  geom_boxplot(mapping = aes(group = cut_width(carat, 0.1)))
```
```{r}
ggplot(data = diamonds, mapping = aes(x = carat, y = price)) + 
  geom_boxplot(mapping = aes(group = cut_number(carat, 20)))
```

```{r}
ggplot(diamonds, x = carat, y = price) +
  geom_boxplot(aes(cut_number(carat, 15)))
```

```{r}
ggplot(data = diamonds) +
  geom_point(mapping = aes(x = x, y = y)) +
  coord_cartesian(xlim = c(4, 11), ylim = c(4, 11))
```

```{r}
ggplot(data = faithful) + 
  geom_point(mapping = aes(x = eruptions, y = waiting))
```

```{r}
library(modelr)

mod <- lm(log(price) ~ log(carat), data = diamonds)

diamonds2 <- diamonds %>% 
  add_residuals(mod) %>% 
  mutate(resid = exp(resid))

ggplot(data = diamonds2) + 
  geom_point(mapping = aes(x = carat, y = resid))
```
```{r}
ggplot(data = diamonds2) + 
  geom_boxplot(mapping = aes(x = cut, y = resid))
```

```{r}
ggplot(data = faithful, mapping = aes(x = eruptions)) + 
  geom_freqpoly(binwidth = 0.25)
```

```{r}
ggplot(faithful, aes(eruptions)) + 
  geom_freqpoly(binwidth = 0.25)
```

```{r}
diamonds %>% 
  count(cut, clarity) %>% 
  ggplot(aes(clarity, cut, fill = n)) + 
    geom_tile()
```

```{r}
tribble(
  ~x, ~y, ~z,
  #--|--|----
  "a", 2, 3.6,
  "b", 1, 8.5
)
```

```{r}
tibble(
  a = lubridate::now() + runif(1e3) * 86400,
  b = lubridate::today() + runif(1e3) * 30,
  c = 1:1e3,
  d = runif(1e3),
  e = sample(letters, 1e3, replace = TRUE)
)
```

```{r}
nycflights13::flights %>% 
  print(n = 10, width = Inf)
```

```{r}
nycflights13::flights %>% 
  View()
```

```{r}
df <- tibble(
  x = runif(5),
  y = rnorm(5),
  df %>% .$x
)
```

```{r}
str(parse_logical(c("TRUE", "FALSE", "NA")))
#>  logi [1:3] TRUE FALSE NA
str(parse_integer(c("1", "2", "3")))
#>  int [1:3] 1 2 3
str(parse_date(c("2010-01-01", "1979-10-14")))
problems()
```
```{r}
parse_double("1.23")
#> [1] 1.23
parse_double("1,23", locale = locale(decimal_mark = ","))
#> [1] 1.23
```

```{r}
parse_number("$100")
parse_number("20%")
parse_number("It cost $123.45")

parse_number("$100")
parse_number("20%")
parse_number("It cost $123.45")

```
```{r}
charToRaw("Hadley")
```

```{r}
x1 <- "El Ni\xf1o was particularly bad this year"
x2 <- "\x82\xb1\x82\xf1\x82\xc9\x82\xbf\x82\xcd"
parse_character(x1, locale = locale(encoding = "Latin1"))
parse_character(x2, locale = locale(encoding = "Shift-JIS"))
```

```{r}
guess_encoding(charToRaw(x1))
guess_encoding(charToRaw(x2))
```

```{r}
fruit <- c("apple", "banana")
parse_factor(c("apple", "banana", "bananana"), levels = fruit)
```

```{r}
parse_datetime("2010-10-01T2010")
parse_datetime("20101010")
parse_date("2010-10-01")
```
```{r}
library("hms")
parse_time("01:10 am")
parse_time("20:10:01")
```

```{r}
parse_date("01/02/15", "%m/%d/%y")
parse_date("01/02/15", "%d/%m/%y")
parse_date("01/02/15", "%y/%m/%d")
```

```{r}
d1 <- "2010-01-01"
d2 <- "2015-03-07"
d3 <- "2017-06-06"
d4 <- c("2015-08-19", "2015-07-01")
d5 <- "2014-12-30" # Dec 30, 2014
t1 <- "1705"
t2 <- "11:15:10.12 PM"
```

```{r}
parse_date(d1)
parse_date(d2)
parse_date(d3)
parse_date(d4)
parse_date(d5)
```

```{r}
guess_parser("2010-10-01")
guess_parser("15:01")
guess_parser(c("TRUE", "FALSE"))
guess_parser(c("1", "5", "9"))
guess_parser(c("12,352,561"))
str(parse_guess("2010-10-10"))
```

```{r}
challenge <- read_csv(readr_example("challenge.csv"))
problems(challenge)
```

```{r}
tail(challenge)
```

```{r}
challenge <- read_csv(
  readr_example("challenge.csv"), 
  col_types = cols(
    x = col_double(),
    y = col_logical()
  )
)
```
```{r}
challenge <- read_csv(
  readr_example("challenge.csv"), 
  col_types = cols(
    x = col_double(),
    y = col_date()
  )
)
tail(challenge)
```

```{r}
challenge2 <- read_csv(readr_example("challenge.csv"), guess_max = 1001)
challenge2 <- read_csv(readr_example("challenge.csv"), 
  col_types = cols(.default = col_character()))
```

```{r}
df <- tribble(
  ~x,  ~y,
  "1", "1.21",
  "2", "2.32",
  "3", "4.56"
)
```

```{r}
table1 %>% 
  mutate(rate = cases / population * 10000)
```

```{r}
print(table4a)
```

```{r}
table4a %>% 
  pivot_longer(c(`1999`, `2000`), names_to = "year", values_to = "cases")
```

```{r}
table4b %>% 
  pivot_longer(c(`1999`, `2000`), names_to = "year", values_to = "population")
```

```{r}
tidy4a <- table4a %>% 
  pivot_longer(c(`1999`, `2000`), names_to = "year", values_to = "cases")
tidy4b <- table4b %>% 
  pivot_longer(c(`1999`, `2000`), names_to = "year", values_to = "population")
left_join(tidy4a, tidy4b)
```

```{r}
table2 %>%
    pivot_wider(names_from = type, values_from = count)
```

```{r}
table3 %>% 
  separate(rate, into = c("cases", "population"), sep = "/")
```

```{r}
table3 %>% 
  separate(rate, into = c("cases", "population"), convert = TRUE)
```

```{r}
table3 %>% 
  separate(year, into = c("century", "year"), sep = 2)
```

```{r}
tibble(x = c("a,b,c", "d,e,f,g", "h,i,j")) %>% 
  separate(x, c("one", "two", "three"))

tibble(x = c("a,b,c", "d,e", "f,g,i")) %>% 
  separate(x, c("one", "two", "three"))
```

```{r}
tibble(x = c("a,b,c", "d,e,f,g", "h,i,j")) %>% 
  separate(x, c("one", "two", "three"))
```

```{r}
stocks <- tibble(
  year   = c(2015, 2015, 2015, 2015, 2016, 2016, 2016),
  qtr    = c(   1,    2,    3,    4,    2,    3,    4),
  return = c(1.88, 0.59, 0.35,   NA, 0.92, 0.17, 2.66))
```

```{r}
stocks %>% 
  pivot_wider(names_from = year, values_from = return)
```

```{r}
stocks %>% 
  pivot_wider(names_from = year, values_from = return) %>% 
  pivot_longer(
    cols = c(`2015`, `2016`), 
    names_to = "year", 
    values_to = "return", 
    values_drop_na = TRUE
  )
```

```{r}
stocks %>% 
  complete(year, qtr)
```

```{r}
treatment <- tribble(
  ~ person,           ~ treatment, ~response,
  "Derrick Whitmore", 1,           7,
  NA,                 2,           10,
  NA,                 3,           9,
  "Katherine Burke",  1,           4
)
treatment %>% 
  fill(person)
```

```{r}
who
```

```{r}
who1 <- who %>% 
  pivot_longer(
    cols = new_sp_m014:newrel_f65, 
    names_to = "key", 
    values_to = "cases", 
    values_drop_na = TRUE)
who1
```

```{r}
who1 %>% 
  count(key)
```

```{r}
who2 <- who1 %>% 
  mutate(key = stringr::str_replace(key, "newrel", "new_rel"))
who2
```

```{r}
who3 <- who2 %>% 
  separate(key, c("new", "type", "sexage"), sep = "_")
who3
```

```{r}
who3 %>% 
  count(new)
who4 <- who3 %>% 
  select(-new, -iso2, -iso3)
```

```{r}
who5 <- who4 %>% 
  separate(sexage, c("sex", "age"), sep = 1)
who5
```

```{r}
who %>%
  pivot_longer(
    cols = new_sp_m014:newrel_f65, 
    names_to = "key", 
    values_to = "cases", 
    values_drop_na = TRUE
  ) %>% 
  mutate(
    key = stringr::str_replace(key, "newrel", "new_rel")
  ) %>%
  separate(key, c("new", "var", "sexage")) %>% 
  select(-new, -iso2, -iso3) %>% 
  separate(sexage, c("sex", "age"), sep = 1)

```



```{r}
planes %>% 
  count(tailnum) %>% 
  filter(n > 1)
weather %>% 
  count(year, month, day, hour, origin) %>% 
  filter(n > 1)
```

```{r}
flights %>% 
  count(year, month, day, flight) %>% 
  filter(n > 1)


flights %>% 
  count(year, month, day, tailnum) %>% 
  filter(n > 1)

```

```{r}
