# DSW Workshop dev

## Szybki setup online

Wchodzicie na stronę, logujecie się i klikacie w napisy jak poniżej.

<https://labs.play-with-docker.com/>

> *START*
>
> \+ ADD NEW INSTANCE

Gonicie w terminalu:

```bash
git clone https://gitlab.com/zielinskipp/dsw-repozytorium-startowe.git
cd dsw-repozytorium-startowe
```

```bash
docker build -t rs-img .
docker run -d -p 8787:8787 -e PASSWORD=rstudio -v $(pwd):/home/rstudio --name rstudio rs-img
```

> OPEN PORT
>[8787](http:://on-the-web-not-here.pl)

user/password = rstudio/rstudio

## Setup lokalny

1. Zainstaluj gita i dockera na swoim laptopie:

* <https://docs.docker.com/desktop/>
* <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>

Na windows jeszcze to (po tym restart komputera):
* <https://docs.docker.com/desktop/windows/wsl/>

2. Załóż konto na <www.gitlab.com>
3. Otwórz terminal/PowerShell 
4. Wykonaj kolejno poniższe komendy (może być wymagane `sudo`)

```{bash}
git clone <adres-repozytorium.git>
cd dsw-repozytorium-startowe

docker build -t rstudio-workshop .

# linux/macos
docker run -d -p 8787:8787 -e PASSWORD=pass -v $(pwd):/home/rstudio --name rstudio rstudio-workshop

# powershell
docker run -d -p 8787:8787 -e PASSWORD=pass -v ${PWD}:/home/rstudio --name rstudio rstudio-workshop
```

6. W przeglądarce internetowej pod adresem `localhost:8787` powinno być dostępne RStudio

```{bash}
username: rstudio
password: pass
```

7. Przydatne komendy po robocie

* `docker stop rstudio` - zatrzymuje rstudio, "wyłącza program"
* `docker start rstudio` - uruchamia rstudio, "włącza program"
